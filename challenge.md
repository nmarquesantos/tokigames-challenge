# Description of Task 

Your task is to create an API to search over available flights.

There are two available flight’s providers:

https://tokigames-challenge.herokuapp.com/api/flights/cheap
https://tokigames-challenge.herokuapp.com/api/flights/business

Your API should aggregate return flights from both providers. Do remember about sorting, filtering and pagination. Upload solution to any remote git repository like github, bitbucket etc.

Preferable technologies:

- Java 11/13
- Spring boot
- Spring WebFlux (Reactive Web Application)

##Reminders/Tips:

- Write Reactive API modal
- Remember about the tests
- Packaging Structure makes a difference
- Keep the code clean
- Optimize the application
- Keep the commit naming strategy consistent
- Do not start if you think you do not have enough time.
- PS: Mocked API is deployed to free version of Heroku, so the first request can take a few more seconds

Good luck and have fun