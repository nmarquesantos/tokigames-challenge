# Instructions

The following technologies were used to develop this solution:
- Gradle
- JDK 13
- Spring Boot & Webflux
- Junit 5 for tests

There are two utility scripts provided:
`runApp.sh` will run the app in dev mode

`runTests.sh` will run all the tests present in the project

## Testing the app

The app runs by default on port 8080.

There is a single endpoint, which can be reached on:

`http://localhost:8080/flights/{departureCity}/{arrivalCity}/{departureDate}/{arrivalDate}`

An example call would be:

`http://localhost:8080/flights/Ankara/Antalya/2019-06-27/2019-07-29`

This would return all flights departing Ankara on 2019-06-27 and landing in Antalya on 2019-07-29
                                                                                       

# Optimizations :(

Due to time constraints and in not delaying the subsmission any further, I've decided to submit it in the best
state I can without delaying it any further.

With that said, given more time, I would:

- Optimize the parameters or add more endpoints. e.g. search  for all flights departing
from city X on day Y

- At code level, I would wrap the search parameters in an object instead, so the method signature
of the services is less prone to change.

- Add integration tests

It was fun though! First project in webflux and I learned a few things regarding reactive programming.