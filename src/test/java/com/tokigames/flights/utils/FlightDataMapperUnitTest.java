package com.tokigames.flights.utils;

import com.tokigames.flights.domain.integration.BusinessFlightData;
import com.tokigames.flights.domain.integration.CheapFlightData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FlightDataMapperUnitTest {

    private static String departure;

    private static String departureTime;

    private static String arrival;

    private static String arrivalTime;

    @BeforeAll
    static void beforeAll() {
        departure = "London";
        departureTime = "1558905556.000000000";

        arrival = "Singapore";
        arrivalTime = "1558902656.000000000";
    }

    @Test
    void fromCheapFlightsShouldMapToFlightData() {

        var input = new CheapFlightData();
        input.setRoute(departure + "-" +  arrival);
        input.setArrival(arrivalTime);
        input.setDeparture(departureTime);

        var output = FlightDataMapper.fromCheapFlights(input);

        assertEquals(departure, output.getDeparture());
        assertEquals(departureTime, output.getDepartureTime());

        assertEquals(arrival, output.getArrival());
        assertEquals(arrivalTime, output.getArrivalTime());
    }

    @Test
    void fromBusinessFlightsShouldMapToFlightData() {
        var input = new BusinessFlightData();
        input.setDeparture(departure);
        input.setDepartureTime(departureTime);
        input.setArrival(arrival);
        input.setArrivalTime(arrivalTime);

        var output = FlightDataMapper.fromBusinessFlights(input);
        assertEquals("Singapore", output.getArrival());
        assertEquals("London", output.getDeparture());
        assertEquals("1558902656.000000000", output.getArrivalTime());
        assertEquals("1558905556.000000000", output.getDepartureTime());
    }
}