package com.tokigames.flights.api;

import com.tokigames.flights.domain.SortingOrder;
import com.tokigames.flights.service.FlightAggregationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import java.util.Collections;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(FlightController.class)
class FlightControllerUnitTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private FlightAggregationService aggregationService;

    private String departureCity;

    private String departureDate;

    private String arrivalCity;

    private String arrivalDate;

    @BeforeEach
    void beforeEach() {
        departureCity = "London";
        departureDate = "2020-01-16";
        arrivalCity = "Singapore";
        arrivalDate = "2020-01-25";
    }

    @Test
    void searchFlights() {
        when(aggregationService.searchAndAggregate(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC)).thenReturn(Flux.fromIterable(Collections.emptyList()));

        webTestClient.get()
                .uri( uri -> uri
                        .path("/flights/{departureCity}/{arrivalCity}/{departureDate}/{arrivalDate}")
                        .build(departureCity, arrivalCity, departureDate, arrivalDate))
                .exchange()
                .expectStatus()
                .isOk();

        verify(aggregationService, times(1)).searchAndAggregate(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC);
    }
}