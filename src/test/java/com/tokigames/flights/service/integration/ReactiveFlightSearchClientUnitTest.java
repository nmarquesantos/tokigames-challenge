package com.tokigames.flights.service.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.tokigames.flights.domain.SortingOrder;
import com.tokigames.flights.domain.integration.BusinessFlightData;
import com.tokigames.flights.domain.integration.BusinessFlightResponse;
import com.tokigames.flights.domain.integration.CheapFlightData;
import com.tokigames.flights.domain.integration.CheapFlightResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestPropertySource(locations="classpath:application.properties")
class ReactiveFlightSearchClientUnitTest {

    private static WireMockServer wireMockServer;

    private static ObjectMapper objectMapper;

    @SpyBean
    private ReactiveFlightSearchClient client;

    private String departureCity;

    private String departureDate;

    private String arrivalCity;

    private String arrivalDate;

    @BeforeAll
    static void beforeAll() {
        objectMapper = new JsonMapper();

        wireMockServer = new WireMockServer(wireMockConfig().port(8089));
        wireMockServer.start();
        configureFor(wireMockServer.port());
    }

    @AfterAll
    static void afterAll() {
        wireMockServer.stop();
    }

    private CheapFlightResponse buildCheapResponse() {
        return new CheapFlightResponse(List.of(
                new CheapFlightData("London-Singapore", "1563588000.000000000", "1563678000.000000000"),
                new CheapFlightData("Barcelona-Lisbon", "1564410656.000000000", "1561627856.000000000")));
    }

    private BusinessFlightResponse buildBusinessResponse() {
        return new BusinessFlightResponse(List.of(
                new BusinessFlightData("London", "1558902656.000000000", "Singapore", "1564410656.000000000"),
                new BusinessFlightData("Barcelona", "1564410656.000000000", "Lisbon", "1561627856.000000000")
        ));
    }

    @Test
    void searchCheapFlightsShouldApplyFilters() throws JsonProcessingException {
        stubFor(get(urlMatching("/cheap"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type","application/json")
                        .withBody(objectMapper.writeValueAsString(buildCheapResponse()))));

        departureCity = "London";
        departureDate = "2019-07-20";
        arrivalCity = "Singapore";
        arrivalDate = "2019-07-21";
        var cheapFlights = client.searchCheapFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC).collectList().block();
        assertEquals(1, cheapFlights.size());
    }

    @Test
    void searchCheapFlightsShouldReturnEmptyFlightListOnError() {
        stubFor(get(urlMatching("/cheap"))
                .willReturn(aResponse()
                        .withStatus(500)
                        .withBody("Internal error")));

        var cheapFlights = client.searchCheapFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC).collectList().block();
        assertEquals(0, cheapFlights.size());
    }

    @Test
    void searchBusinessFlightsShouldApplyFilters() throws JsonProcessingException {
        stubFor(get(urlMatching("/business"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type","application/json")
                        .withBody(objectMapper.writeValueAsString(buildBusinessResponse()))));

        departureCity = "London";
        departureDate = "2019-05-26";
        arrivalCity = "Singapore";
        arrivalDate = "2019-07-29";

        var businessFlights = client.searchBusinessFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC).collectList().block();
        assertEquals(1, businessFlights.size());
    }

    @Test
    void searchBusinessFlightsShouldReturnEmptyFlightListOnError() {
        stubFor(get(urlMatching("/business"))
                .willReturn(aResponse()
                        .withStatus(500)
                        .withBody("Internal error")));

        var businessFlights = client.searchBusinessFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC).collectList().block();
        assertEquals(0, businessFlights.size());
    }



}