package com.tokigames.flights.service;

import com.tokigames.flights.domain.FlightData;
import com.tokigames.flights.domain.SortingOrder;
import com.tokigames.flights.service.integration.ReactiveFlightSearchClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ReactiveFlightAggregationServiceUnitTest {

    @SpyBean
    private ReactiveFlightAggregationService service;

    @MockBean
    private ReactiveFlightSearchClient searchClient;

    private String departureCity;

    private String departureDate;

    private String arrivalCity;

    private String arrivalDate;

    private Flux<FlightData> buildFlights(int numberOfFlights) {
        var flights = new ArrayList<FlightData>();

        for (int i = 0; i < numberOfFlights; i++) {
            flights.add(new FlightData(String.valueOf(i), String.valueOf(i), String.valueOf(i), String.valueOf(i)));
        }

        return Flux.fromIterable(flights);
    }

    @Test
    void searchAndAggregateReturnsCheapAndBusinessFlights() {

        when(searchClient.searchCheapFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC)).thenReturn(buildFlights(5));

        when(searchClient.searchBusinessFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC)).thenReturn(buildFlights(3));

        var totalFlights = service.searchAndAggregate(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC).collectList().block();

        verify(searchClient, times(1)).searchBusinessFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC);

        verify(searchClient, times(1)).searchCheapFlights(departureCity, departureDate, arrivalCity, arrivalDate, SortingOrder.ASC);

        assertEquals(8, totalFlights.size());
    }
}