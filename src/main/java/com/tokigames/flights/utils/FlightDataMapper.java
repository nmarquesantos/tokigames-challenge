package com.tokigames.flights.utils;

import com.tokigames.flights.domain.FlightData;
import com.tokigames.flights.domain.integration.BusinessFlightData;
import com.tokigames.flights.domain.integration.CheapFlightData;

public class FlightDataMapper {

    public static FlightData fromCheapFlights(CheapFlightData data) {
        var arrivalTime = data.getArrival();
        var departureTime = data.getDeparture();

        var route = data.getRoute().split("-");

        var departure = route[0];
        var arrival = route[1];

        return new FlightData(arrival, departure, arrivalTime, departureTime);
    }

    public static FlightData fromBusinessFlights(BusinessFlightData data) {

        return new FlightData(data.getArrival(), data.getDeparture(), data.getArrivalTime(), data.getDepartureTime());
    }
}
