package com.tokigames.flights.domain;

import java.util.List;

public class FlightResponse {

    private List<FlightData> data;

    public FlightResponse() {}

    public List<FlightData> getData() {
        return data;
    }

    public void setData(List<FlightData> data) {
        this.data = data;
    }
}
