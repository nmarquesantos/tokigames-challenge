package com.tokigames.flights.domain;

public enum SortingOrder {
    ASC,
    DESC
}