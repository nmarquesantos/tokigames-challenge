package com.tokigames.flights.domain;

import com.fasterxml.jackson.annotation.JsonCreator;

public class FlightData {

    private String arrival;

    private String departure;

    private String arrivalTime;

    private String departureTime;

    @JsonCreator
    public FlightData(String arrival, String departure, String arrivalTime, String departureTime) {
        this.arrival = arrival;
        this.departure = departure;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
    }


    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }
}
