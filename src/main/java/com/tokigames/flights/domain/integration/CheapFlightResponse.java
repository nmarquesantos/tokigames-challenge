package com.tokigames.flights.domain.integration;

import java.util.Collections;
import java.util.List;

public class CheapFlightResponse {

    private List<CheapFlightData> data;

    public CheapFlightResponse(List<CheapFlightData> data) {
        this.data = data;
    }

    public CheapFlightResponse() {
        this.data = Collections.emptyList();
    }

    public List<CheapFlightData> getData() {
        return data;
    }

    public void setData(List<CheapFlightData> data) {
        this.data = data;
    }
}
