package com.tokigames.flights.domain.integration;

import java.util.Collections;
import java.util.List;

public class BusinessFlightResponse {

    private List<BusinessFlightData> data;

    public BusinessFlightResponse() {
        this.data = Collections.emptyList();
    }

    public BusinessFlightResponse(List<BusinessFlightData> data) {
        this.data = data;
    }

    public List<BusinessFlightData> getData() {
        return data;
    }
}
