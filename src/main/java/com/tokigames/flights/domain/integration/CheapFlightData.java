package com.tokigames.flights.domain.integration;

public class CheapFlightData {

    private String route;

    private String departure;

    private String arrival;

    public CheapFlightData(String route, String departure, String arrival) {
        this.route = route;
        this.departure = departure;
        this.arrival = arrival;
    }

    public CheapFlightData(){}

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }
}
