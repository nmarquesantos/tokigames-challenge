package com.tokigames.flights.api;

import com.tokigames.flights.domain.FlightData;
import com.tokigames.flights.domain.SortingOrder;
import com.tokigames.flights.service.FlightAggregationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/flights")
public class FlightController {

    private FlightAggregationService aggregationService;

    @Autowired
    public FlightController(FlightAggregationService aggregationService) {
        this.aggregationService = aggregationService;
    }

    @GetMapping("/{departureCity}/{arrivalCity}/{departureDate}/{arrivalDate}")
    public Flux<FlightData> searchFlights(@PathVariable String departureCity,
                                          @PathVariable String departureDate,
                                          @PathVariable String arrivalCity,
                                          @PathVariable String arrivalDate,
                                          @RequestParam(required = false, defaultValue = "ASC") SortingOrder sort) {

        return aggregationService.searchAndAggregate(departureCity, departureDate, arrivalCity, arrivalDate, sort);
    }
}