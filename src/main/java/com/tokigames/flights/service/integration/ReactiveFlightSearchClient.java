package com.tokigames.flights.service.integration;

import com.tokigames.flights.domain.FlightData;
import com.tokigames.flights.domain.SortingOrder;
import com.tokigames.flights.domain.integration.BusinessFlightResponse;
import com.tokigames.flights.domain.integration.CheapFlightResponse;
import com.tokigames.flights.utils.FlightDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Comparator;

@Service
public class ReactiveFlightSearchClient implements FlightSearchClient {

    private WebClient webClient;

    @Autowired
    public ReactiveFlightSearchClient(@Value("${flights.search.url}") String url) {
        this.webClient = WebClient.create(url);
    }

    @Override
    public Flux<FlightData> searchCheapFlights(String departureCity, String departureDate, String arrivalCity, String arrivalDate, SortingOrder sortingOrder) {
        Comparator<FlightData> flightDataComparator = (f1, f2) -> f1.getDepartureTime().compareTo(f2.getDepartureTime());

        if (sortingOrder.equals(SortingOrder.DESC)) {
            flightDataComparator = flightDataComparator.reversed();
        }

        return webClient.get()
                .uri("/cheap")
                .retrieve()
                .bodyToMono(CheapFlightResponse.class)
                .onErrorReturn(new CheapFlightResponse())
                .flatMapMany(response -> Flux.fromIterable(response.getData()))
                .map(FlightDataMapper::fromCheapFlights)
                .filter(data -> filterFlightDate(data.getDepartureTime(), departureDate)
                        && filterFlightDate(data.getArrivalTime(), arrivalDate)
                        && data.getDeparture().equalsIgnoreCase(departureCity)
                        && data.getArrival().equalsIgnoreCase(arrivalCity))
                .sort(flightDataComparator);
    }

    @Override
    public Flux<FlightData> searchBusinessFlights(String departureCity, String departureDate, String arrivalCity, String arrivalDate, SortingOrder sortingOrder) {
        Comparator<FlightData> flightDataComparator = (f1, f2) -> f1.getDepartureTime().compareTo(f2.getDepartureTime());;

        if (sortingOrder.equals(SortingOrder.DESC)) {
            flightDataComparator = flightDataComparator.reversed();
        }

        return webClient.get()
                .uri("/business")
                .retrieve()
                .bodyToMono(BusinessFlightResponse.class)
                .onErrorReturn(new BusinessFlightResponse())
                .flatMapMany(response -> Flux.fromIterable(response.getData()))
                .map(FlightDataMapper::fromBusinessFlights)
                .filter(data -> filterFlightDate(data.getDepartureTime(), departureDate)
                        && filterFlightDate(data.getArrivalTime(), arrivalDate)
                        && data.getDeparture().equalsIgnoreCase(departureCity)
                        && data.getArrival().equalsIgnoreCase(arrivalCity))
                .sort(flightDataComparator);
    }

    private boolean filterFlightDate(String flightDateString, String desiredDateString) {

        /**
         *  For the purpose of the challenge and the search implementation, the nanoseconds
         *  are not relevant as we are only using precision up until the day.
         * */
        var decimalTimestamp = Math.floor(Double.parseDouble(flightDateString));
        var roundedTimestamp = Math.round(decimalTimestamp);
        var instantWithNanos = Instant.ofEpochSecond(roundedTimestamp);

        var flightDate = LocalDate.ofInstant(instantWithNanos, ZoneId.systemDefault());
        var desiredDate = LocalDate.parse(desiredDateString);

        return desiredDate.isEqual(flightDate);
    }

}
