package com.tokigames.flights.service.integration;

import com.tokigames.flights.domain.FlightData;
import com.tokigames.flights.domain.SortingOrder;
import reactor.core.publisher.Flux;

public interface FlightSearchClient {

    Flux<FlightData> searchCheapFlights(String departureCity, String departureDate, String arrivalCity, String arrivalDate, SortingOrder sortingOrder);

    Flux<FlightData> searchBusinessFlights(String departureCity, String departureDate, String arrivalCity, String arrivalDate, SortingOrder sortingOrder);
}