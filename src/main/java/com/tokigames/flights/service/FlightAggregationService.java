package com.tokigames.flights.service;

import com.tokigames.flights.domain.FlightData;
import com.tokigames.flights.domain.SortingOrder;
import reactor.core.publisher.Flux;

public interface FlightAggregationService {

    Flux<FlightData> searchAndAggregate(String departureCity,
                                        String departureDate,
                                        String arrivalCity,
                                        String arrivalDate,
                                        SortingOrder sortingOrder);
}
