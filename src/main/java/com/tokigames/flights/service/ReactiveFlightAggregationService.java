package com.tokigames.flights.service;

import com.tokigames.flights.domain.FlightData;
import com.tokigames.flights.domain.SortingOrder;
import com.tokigames.flights.service.integration.FlightSearchClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class ReactiveFlightAggregationService implements FlightAggregationService {

    private FlightSearchClient flightSearchClient;

    @Autowired
    public ReactiveFlightAggregationService(FlightSearchClient flightSearchClient) {
        this.flightSearchClient = flightSearchClient;
    }

    @Override
    public Flux<FlightData> searchAndAggregate(String departureCity,
                                               String departureDate,
                                               String arrivalCity,
                                               String arrivalDate,
                                               SortingOrder sortingOrder) {

        var businessFlights = flightSearchClient.searchBusinessFlights(departureCity, departureDate, arrivalCity, arrivalDate, sortingOrder);
        var cheapFlights = flightSearchClient.searchCheapFlights(departureCity, departureDate, arrivalCity, arrivalDate, sortingOrder);

        return Flux.merge(businessFlights, cheapFlights);
    }
}
